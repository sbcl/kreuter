;;; -*- lisp -*-
;;;
;;; **********************************************************************
;;; This code was written by Paul Foley and has been placed in the public
;;; domain.
;;;

;;; Sbcl port by Rudi Schlatte.

(in-package "SB-SIMPLE-STREAMS")

;;;
;;; **********************************************************************
;;;
;;; Definition of File-Simple-Stream and relations

(def-stream-class file-simple-stream (single-channel-simple-stream file-stream)
  (;; XXX: I can't tell if it's part of the external API that we call
   ;; the name used to open the file the "filename", but this is at
   ;; variance with other uses of the term "filename" around SBCL,
   ;; where we mostly mean "string suitable for system calls".  --
   ;; RMK, 2008-01-04.
   (filename :initform nil :initarg :filename)
   (truename :initform nil :initarg :truename)
   (altname :initform nil :initarg :altname)
   (after-close :initform nil :initarg :after-close)
   (owner-pid :initform nil :initarg :owner-pid)))

(sb-ext:with-unlocked-packages ("SB-IMPL" "SB-EXT")
  (fmakunbound 'stream-pathname)
  (defgeneric stream-pathname (stream)
    (:method ((stream sb-sys:fd-stream))
      (sb-impl::fd-stream-pathname stream))
    (:method ((stream file-simple-stream))
      (with-stream-class (file-simple-stream stream)
        (sm filename stream))))

  (fmakunbound 'stream-truename)
  (defgeneric stream-truename (stream)
    (:method ((stream sb-sys:fd-stream))
      (let ((truename (sb-impl::fd-stream-truename stream)))
        (if truename
            truename
            ;; This is a flaw in our system: not all FD-STREAMs should be
            ;; FILE-STREAMs, Unix propaganda notwithstanding.
            (error 'type-error :datum stream :expected-type 'file-stream))))
    (:method ((stream file-simple-stream))
      (with-stream-class (file-simple-stream stream)
        (sm truename stream))))

  (fmakunbound '(setf stream-truename))
  (defgeneric (setf stream-truename) (new-value stream)
    (:method  (new-name (stream sb-sys:fd-stream))
      (setf (sb-impl::fd-stream-truename stream) new-name))
    (:method (new-name (stream file-simple-stream))
      (with-stream-class (file-simple-stream stream)
        (setf (sm truename stream) new-name))))

  (fmakunbound 'stream-altname)
  (defgeneric stream-altname (stream)
    (:method ((stream sb-sys:fd-stream))
      (sb-impl::fd-stream-altname stream))
    (:method ((stream file-simple-stream))
      (with-stream-class (file-simple-stream stream)
        (sm altname stream))))

  (fmakunbound '(setf stream-altname))
  (defgeneric (setf stream-altname) (new-value stream)
    (:method  (new-name (stream sb-sys:fd-stream))
      (setf (sb-impl::fd-stream-altname stream) new-name))
    (:method (new-name (stream file-simple-stream))
      (with-stream-class (file-simple-stream stream)
        (setf (sm altname stream) new-name))))

  (fmakunbound 'stream-owner-pid)
  (defgeneric stream-owner-pid (stream)
    (:method ((stream sb-sys:fd-stream))
      (sb-impl::fd-stream-owner-pid stream))
    (:method ((stream file-simple-stream))
      (with-stream-class (file-simple-stream stream)
        (sm owner-pid stream))))

  (fmakunbound '(setf stream-owner-pid))
  (defgeneric (setf stream-owner-pid) (new-value stream)
    (:method  (new-name (stream sb-sys:fd-stream))
      (setf (sb-impl::fd-stream-owner-pid stream) new-name))
    (:method (new-name (stream file-simple-stream))
      (with-stream-class (file-simple-stream stream)
        (setf (sm owner-pid stream) new-name))))

  (fmakunbound 'stream-after-close)
  (defgeneric stream-after-close (stream)
    (:method ((stream sb-sys:fd-stream))
      (sb-impl::fd-stream-after-close stream))
    (:method ((stream file-simple-stream))
      (with-stream-class (file-simple-stream stream)
        (sm after-close stream))))

  (fmakunbound '(setf stream-after-close))
  (defgeneric (setf stream-after-close) (new-value stream)
    (:method  (new-name (stream sb-sys:fd-stream))
      (setf (sb-impl::fd-stream-after-close stream) new-name))
    (:method (new-name (stream file-simple-stream))
      (with-stream-class (file-simple-stream stream)
        (setf (sm after-close stream) new-name)))))

(def-stream-class mapped-file-simple-stream (file-simple-stream
                                             direct-simple-stream)
  ())

(def-stream-class probe-simple-stream (simple-stream)
  ((pathname :initform nil :initarg :pathname)))

(defmethod print-object ((object file-simple-stream) stream)
  (print-unreadable-object (object stream :type nil :identity nil)
    (with-stream-class (file-simple-stream object)
      (cond ((not (any-stream-instance-flags object :simple))
             (princ "Invalid " stream))
            ((not (any-stream-instance-flags object :input :output))
             (princ "Closed " stream)))
      (format stream "~:(~A~) for ~S"
              (type-of object) (sm filename object)))))


(sb-ext:with-unlocked-packages ("SB-IMPL")
  (defun open-file-stream (stream options)
    (let ((direction (getf options :direction :input))
          (if-exists (getf options :if-exists))
          (if-exists-given (not (eql (getf options :if-exists t) t)))
          (if-does-not-exist (getf options :if-does-not-exist))
          (if-does-not-exist-given (not (eql (getf options :if-does-not-exist t) t))))
      (with-stream-class (file-simple-stream stream)
        (ecase direction
          (:input (add-stream-instance-flags stream :input))
          (:output (add-stream-instance-flags stream :output))
          (:io (add-stream-instance-flags stream :input :output)))
        (cond ((and (sm input-handle stream) (sm output-handle stream)
                    (not (eql (sm input-handle stream)
                              (sm output-handle stream))))
               (error "Input-Handle and Output-Handle can't be different."))
              ((or (sm input-handle stream) (sm output-handle stream))
               (add-stream-instance-flags stream :simple)
               ;; get namestring, etc., from handle, if possible
               ;;    (i.e., if it's a stream)
               ;; set up buffers
               stream)
              (t
               (let (*file-descriptor*)   ;OPEN-FILE sets this
                 (declare (special *file-descriptor*))
                 (multiple-value-bind
                       (pathname truename altname input output
                                 init-func close-func)
                     (open-file
                      (getf options :filename)
                      direction
                      (if if-does-not-exist-given
                          if-does-not-exist
                          'default)
                      (if if-exists-given
                          (case if-exists
                            ;; KLUDGE: circa 1.0.13, SB-SIMPLE-STREAMS
                            ;; did :APPEND as an O_RDWR opening followed
                            ;; by a reposition, but FD-STREAMS opened
                            ;; with O_APPEND.
                                        ;(:append :overwrite)
                            (otherwise if-exists))
                          'default)
                      ;; FIXME: SIMPLE-STREAMS ignores ELEMENT-TYPE, and
                      ;; OPEN-FILE doesn't care about the ELEMENT-TYPE,
                      ;; but in principle we might have to care about it
                      ;; eventually on Windows.
                      '(unsigned-byte 8)
                      (apply #'sb-sys:make-os-open-arguments options))
                   (declare (ignore input output))
                   (unwind-protect
                        (when *file-descriptor*
                          (add-stream-instance-flags stream :simple)
                          (setf (sm filename stream) pathname
                                (sm truename stream) truename
                                (sm altname stream) altname
                                (sm after-close stream) close-func
                                (sm owner-pid stream) (sb-posix:getpid))
                          (when (any-stream-instance-flags stream :input)
                            (setf (sm input-handle stream)
                                  *file-descriptor*))
                          (when (any-stream-instance-flags stream :output)
                            (setf (sm output-handle stream)
                                  *file-descriptor*))
                          (sb-ext:finalize stream
                                           (lambda ()
                                             (close-descriptor
                                              *file-descriptor*)
                                             (format
                                              *terminal-io*
                                              "~&;;; ** closed ~A (fd ~D)~%"
                                              pathname
                                              *file-descriptor*)))
                          (setf *file-descriptor* nil)
                          (when init-func
                            (handler-case (funcall init-func stream)
                              (error () (close stream :abort t))))
                          stream)
                     (when *file-descriptor*
                       (sb-posix:close *file-descriptor*)))))))))))

(defmethod device-open ((stream file-simple-stream) options)
  (with-stream-class (file-simple-stream stream)
    (when (open-file-stream stream options)
      ;; Franz says:
      ;;  "The device-open method must be prepared to recognize resource
      ;;   and change-class situations. If no filename is specified in
      ;;   the options list, and if no input-handle or output-handle is
      ;;   given, then the input-handle and output-handle slots should
      ;;   be examined; if non-nil, that means the stream is still open,
      ;;   and thus the operation being requested of device-open is a
      ;;   change-class. Also, a device-open method need not allocate a
      ;;   buffer every time it is called, but may instead reuse a
      ;;   buffer it finds in a stream, if it does not become a security
      ;;   issue."
      (unless (sm buffer stream)
        (let ((length (device-buffer-length stream)))
          (setf (sm buffer stream) (allocate-buffer length)
                (sm buffpos stream) 0
                (sm buffer-ptr stream) 0
                (sm buf-len stream) length)))
      (when (any-stream-instance-flags stream :output)
        (setf (sm control-out stream) *std-control-out-table*))
      (setf (stream-external-format stream)
            (getf options :external-format :default))
      stream)))

(defmethod device-close ((stream file-simple-stream) abort)
  (when (open-stream-p stream)
    (with-stream-class (file-simple-stream stream)
      (let ((fd (or (sm input-handle stream) (sm output-handle stream))))
        (when (sb-int:fixnump fd)
          (close-descriptor fd
                            (lambda (fd errno)
                              (declare (ignore fd))
                              (sb-impl::simple-stream-perror
                               "failed to close() the descriptor in ~A"
                               stream errno)))
          (do-after-close-actions stream abort))
        (when (sm buffer stream)
          (free-buffer (sm buffer stream))
          (setf (sm buffer stream) nil)))))
  t)

(defmethod device-file-position ((stream file-simple-stream))
  (with-stream-class (file-simple-stream stream)
    (let ((fd (or (sm input-handle stream) (sm output-handle stream))))
      (if (sb-int:fixnump fd)
          (values (sb-unix:unix-lseek fd 0 sb-unix:l_incr))
          (file-position fd)))))

(defmethod (setf device-file-position) (value (stream file-simple-stream))
  (declare (type fixnum value))
  (with-stream-class (file-simple-stream stream)
    (let ((fd (or (sm input-handle stream) (sm output-handle stream))))
      (if (sb-int:fixnump fd)
          (values (sb-unix:unix-lseek fd
                                      (if (minusp value) (1+ value) value)
                                      (if (minusp value) sb-unix:l_xtnd sb-unix:l_set)))
          (file-position fd value)))))

(defmethod device-file-length ((stream file-simple-stream))
  (with-stream-class (file-simple-stream stream)
    (let ((fd (or (sm input-handle stream) (sm output-handle stream))))
      (if (sb-int:fixnump fd)
          (multiple-value-bind (okay dev ino mode nlink uid gid rdev size)
              (sb-unix:unix-fstat (sm input-handle stream))
            (declare (ignore dev ino mode nlink uid gid rdev))
            (if okay size nil))
          (file-length fd)))))

(defmethod device-open ((stream mapped-file-simple-stream) options)
  (with-stream-class (mapped-file-simple-stream stream)
    (when (open-file-stream stream options)
      (let* ((input (any-stream-instance-flags stream :input))
             (output (any-stream-instance-flags stream :output))
             (prot (logior (if input sb-posix::PROT-READ 0)
                           (if output sb-posix::PROT-WRITE 0)))
             (fd (or (sm input-handle stream) (sm output-handle stream))))
        (unless (sb-int:fixnump fd)
          (error "Can't memory-map an encapsulated stream."))
        (multiple-value-bind (okay dev ino mode nlink uid gid rdev size)
            (sb-unix:unix-fstat fd)
          (declare (ignore ino mode nlink uid gid rdev))
          (unless okay
            (sb-unix:unix-close fd)
            (sb-ext:cancel-finalization stream)
            (error "Error fstating ~S: ~A" stream
                   (sb-int:strerror dev)))
          (when (>= size most-positive-fixnum)
            ;; Or else BUF-LEN has to be a general integer, or
            ;; maybe (unsigned-byte 32).  In any case, this means
            ;; BUF-MAX and BUF-PTR have to be the same, which means
            ;; number-consing every time BUF-PTR moves...
            ;; Probably don't have the address space available to map
            ;; bigger files, anyway.  Maybe DEVICE-READ can adjust
            ;; the mapped portion of the file when necessary?
            (warn "Unable to memory-map entire file.")
            (setf size (1- most-positive-fixnum)))
          (let ((buffer
                 (handler-case
                  (sb-posix:mmap nil size prot sb-posix::MAP-SHARED fd 0)
                  (sb-posix:syscall-error nil))))
            (when (null buffer)
              (sb-unix:unix-close fd)
              (sb-ext:cancel-finalization stream)
              (error "Unable to map file."))
            (setf (sm buffer stream) buffer
                  (sm buffpos stream) 0
                  (sm buffer-ptr stream) size
                  (sm buf-len stream) size)
            (when (any-stream-instance-flags stream :output)
              (setf (sm control-out stream) *std-control-out-table*))
            (let ((efmt (getf options :external-format :default)))
              (compose-encapsulating-streams stream efmt)
              (setf (stream-external-format stream) efmt)
              ;; overwrite the strategy installed in :after method of
              ;; (setf stream-external-format)
              (install-single-channel-character-strategy
               (melding-stream stream) efmt 'mapped))
            (sb-ext:finalize stream
              (lambda ()
                (sb-posix:munmap buffer size)
                (format *terminal-io* "~&;;; ** unmapped ~S" buffer))))))
      stream)))


(defmethod device-close ((stream mapped-file-simple-stream) abort)
  (with-stream-class (mapped-file-simple-stream stream)
    (when (sm buffer stream)
      (sb-posix:munmap (sm buffer stream) (sm buf-len stream))
      (setf (sm buffer stream) nil))
    (sb-unix:unix-close (or (sm input-handle stream) (sm output-handle stream))))
  t)

(defmethod device-write ((stream mapped-file-simple-stream) buffer
                         start end blocking)
  (assert (eq buffer :flush) (buffer)) ; finish/force-output
  (with-stream-class (mapped-file-simple-stream stream)
    (sb-posix:msync (sm buffer stream) (sm buf-len stream)
                    (if blocking sb-posix::ms-sync sb-posix::ms-async))))

(defmethod device-open ((stream probe-simple-stream) options)
  (let ((pathname (getf options :filename)))
    (with-stream-class (probe-simple-stream stream)
      (add-stream-instance-flags stream :simple)
      (when (sb-unix:unix-access (sb-int:unix-namestring pathname nil) sb-unix:f_ok)
        (setf (sm filename stream) pathname)
        t))))

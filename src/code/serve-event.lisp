;;;; This software is part of the SBCL system. See the README file for
;;;; more information.
;;;;
;;;; This software is derived from the CMU CL system, which was
;;;; written at Carnegie Mellon University and released into the
;;;; public domain. The software is in the public domain and is
;;;; provided with absolutely no warranty. See the COPYING and CREDITS
;;;; files for more information.

(in-package "SB!IMPL")

;;;; file descriptor I/O noise

(defstruct (handler
            (:constructor make-handler (direction descriptor function))
            (:copier nil))
  ;; Reading or writing...
  (direction nil :type (member :input :output))
  ;; File descriptor this handler is tied to.
  (descriptor 0 :type #!-win32-uses-file-handles (mod #.sb!unix:fd-setsize)
              #!+win32-uses-file-handles fixnum)
  ;; T iff this handler is running.
  ;;
  ;; FIXME: unused. At some point this used to be set to T
  ;; around the call to the handler-function, but that was commented
  ;; out with the verbose explantion "Doesn't work -- ACK".
  active
  ;; Function to call.
  (function nil :type function)
  ;; T if this descriptor is bogus.
  bogus)

(def!method print-object ((handler handler) stream)
  (print-unreadable-object (handler stream :type t)
    (format stream
            "~A on ~:[~;BOGUS ~]descriptor ~W: ~S"
            (handler-direction handler)
            (handler-bogus handler)
            (handler-descriptor handler)
            (handler-function handler))))

(defvar *descriptor-handlers* nil
  #!+sb-doc
  "List of all the currently active handlers for file descriptors")

(sb!xc:defmacro with-descriptor-handlers (&body forms)
  ;; FD-STREAM functionality can add and remove descriptors on it's
  ;; own, so getting an interrupt while modifying this and the
  ;; starting to recursively modify it could lose...
  `(without-interrupts ,@forms))

(defun list-all-descriptor-handlers ()
  (with-descriptor-handlers
    (copy-list *descriptor-handlers*)))

(defun select-descriptor-handlers (function)
  (declare (function function))
  (with-descriptor-handlers
    (remove-if-not function *descriptor-handlers*)))

(defun map-descriptor-handlers (function)
  (declare (function function))
  (with-descriptor-handlers
    (dolist (handler *descriptor-handlers*)
      (funcall function handler))))

;;; Add a new handler to *descriptor-handlers*.
(defun add-fd-handler (fd direction function)
  #!+sb-doc
  "Arange to call FUNCTION whenever FD is usable. DIRECTION should be
  either :INPUT or :OUTPUT. The value returned should be passed to
  SYSTEM:REMOVE-FD-HANDLER when it is no longer needed."
  (unless (member direction '(:input :output))
    ;; FIXME: should be TYPE-ERROR?
    (error "Invalid direction ~S, must be either :INPUT or :OUTPUT" direction))
  (let ((handler (make-handler direction fd function)))
    (with-descriptor-handlers
      (push handler *descriptor-handlers*))
    handler))

;;; Remove an old handler from *descriptor-handlers*.
(defun remove-fd-handler (handler)
  #!+sb-doc
  "Removes HANDLER from the list of active handlers."
  (with-descriptor-handlers
    (setf *descriptor-handlers*
          (delete handler *descriptor-handlers*))))

;;; Search *descriptor-handlers* for any reference to fd, and nuke 'em.
(defun invalidate-descriptor (fd)
  #!+sb-doc
  "Remove any handers refering to fd. This should only be used when attempting
  to recover from a detected inconsistancy."
  (with-descriptor-handlers
    (setf *descriptor-handlers*
          (delete fd *descriptor-handlers*
                  :key #'handler-descriptor))))

;;; Add the handler to *descriptor-handlers* for the duration of BODY.
(defmacro with-fd-handler ((fd direction function) &rest body)
  #!+sb-doc
  "Establish a handler with SYSTEM:ADD-FD-HANDLER for the duration of BODY.
   DIRECTION should be either :INPUT or :OUTPUT, FD is the file descriptor to
   use, and FUNCTION is the function to call whenever FD is usable."
  (let ((handler (gensym)))
    `(let (,handler)
       (unwind-protect
           (progn
             (setf ,handler (add-fd-handler ,fd ,direction ,function))
             ,@body)
         (when ,handler
           (remove-fd-handler ,handler))))))

;;; First, get a list and mark bad file descriptors. Then signal an error
;;; offering a few restarts.
(defun handler-descriptors-error ()
  (let ((bogus-handlers nil))
    (dolist (handler (list-all-descriptor-handlers))
      (unless (or (handler-bogus handler)
                  (sb!unix:unix-fstat (handler-descriptor handler)))
        (setf (handler-bogus handler) t)
        (push handler bogus-handlers)))
    (restart-case (error "~S ~[have~;has a~:;have~] bad file descriptor~:P."
                         bogus-handlers (length bogus-handlers))
      (remove-them ()
        :report "Remove bogus handlers."
        (with-descriptor-handlers
          (setf *descriptor-handlers*
                (delete-if #'handler-bogus *descriptor-handlers*))))
      (retry-them ()
        :report "Retry bogus handlers."
       (dolist (handler bogus-handlers)
         (setf (handler-bogus handler) nil)))
      (continue ()
        :report "Go on, leaving handlers marked as bogus.")))
  nil)


;;;; SERVE-ALL-EVENTS, SERVE-EVENT, and friends

;;; Wait until FD is usable for DIRECTION. The timeout given to serve-event is
;;; recalculated each time through the loop so that WAIT-UNTIL-FD-USABLE will
;;; timeout at the correct time irrespective of how many events are handled in
;;; the meantime.
(defun wait-until-fd-usable (fd direction &optional timeout)
  #!+sb-doc
  "Wait until FD is usable for DIRECTION. DIRECTION should be either :INPUT or
:OUTPUT. TIMEOUT, if supplied, is the number of seconds to wait before giving
up."
  (prog (usable)
   :restart
     (multiple-value-bind (to-sec to-usec stop-sec stop-usec signalp)
         (decode-timeout timeout)
       (declare (type (or integer null) to-sec to-usec))
       (with-fd-handler (fd direction (lambda (fd)
                                        (declare (ignore fd))
                                        (setf usable t)))
         (loop
           (sub-serve-event to-sec to-usec signalp)
           (when usable
             (return-from wait-until-fd-usable t))
           (when to-sec
             (multiple-value-bind (sec usec)
                 (decode-internal-time (get-internal-real-time))
               (setf to-sec (- stop-sec sec))
               (cond ((> usec stop-usec)
                      (decf to-sec)
                      (setf to-usec (- (+ stop-usec 1000000) usec)))
                     (t
                      (setf to-usec (- stop-usec usec)))))
             (when (or (minusp to-sec) (minusp to-usec))
               (if signalp
                   (progn
                     (signal-deadline)
                     (go :restart))
                   (return-from wait-until-fd-usable nil)))))))))

;;; Wait for up to timeout seconds for an event to happen. Make sure all
;;; pending events are processed before returning.
(defun serve-all-events (&optional timeout)
  #!+sb-doc
  "SERVE-ALL-EVENTS calls SERVE-EVENT with the specified timeout. If
SERVE-EVENT does something (returns T) it loops over SERVE-EVENT with a
timeout of 0 until there are no more events to serve. SERVE-ALL-EVENTS returns
T if SERVE-EVENT did something and NIL if not."
  (do ((res nil)
       (sval (serve-event timeout) (serve-event 0)))
      ((null sval) res)
    (setq res t)))

;;; Serve a single set of events.
(defun serve-event (&optional timeout)
  #!+sb-doc
  "Receive pending events on all FD-STREAMS and dispatch to the appropriate
handler functions. If timeout is specified, server will wait the specified
time (in seconds) and then return, otherwise it will wait until something
happens. Server returns T if something happened and NIL otherwise. Timeout
0 means polling without waiting."
  (multiple-value-bind (to-sec to-usec stop-sec stop-usec signalp)
      (decode-timeout timeout)
    (declare (ignore stop-sec stop-usec))
    (sub-serve-event to-sec to-usec signalp)))

;;; When a *periodic-polling-function* is defined the server will not
;;; block for more than the maximum event timeout and will call the
;;; polling function if it does time out.
(declaim (type (or null symbol function) *periodic-polling-function*))
(defvar *periodic-polling-function* nil
  "Either NIL, or a designator for a function callable without any
arguments. Called when the system has been waiting for input for
longer then *PERIODIC-POLLING-PERIOD* seconds. Shared between all
threads, unless locally bound. EXPERIMENTAL.")
(declaim (real *periodic-polling-period*))
(defvar *periodic-polling-period* 0
  "A real number designating the number of seconds to wait for input
at maximum, before calling the *PERIODIC-POLLING-FUNCTION* \(if any.)
Shared between all threads, unless locally bound. EXPERIMENTAL.")

;;; Takes timeout broken into seconds and microseconds.
#!-win32-uses-file-handles
(defun sub-serve-event (to-sec to-usec deadlinep)
  ;; Figure out our peridic polling needs. MORE-SEC/USEC is the amount
  ;; of actual waiting left after we poll (assuming we are polling.)
  (multiple-value-bind (poll more-sec more-usec)
      (when *periodic-polling-function*
        (multiple-value-bind (p-sec p-usec)
            (decode-internal-time
             (seconds-to-internal-time *periodic-polling-period*))
          (when (or (not to-sec) (> to-sec p-sec)
                    (and (= to-sec p-sec) (> to-usec p-usec)))
            (multiple-value-prog1
                (values *periodic-polling-function*
                        (when to-sec (- to-sec p-sec))
                        (when to-sec (- to-usec p-usec)))
              (setf to-sec p-sec
                    to-usec p-sec)))))

    ;; Next, wait for something to happen.
    (sb!alien:with-alien ((read-fds (sb!alien:struct sb!unix:fd-set))
                            (write-fds (sb!alien:struct sb!unix:fd-set)))
        (sb!unix:fd-zero read-fds)
        (sb!unix:fd-zero write-fds)
        (let ((count 0))
          (declare (type index count))

          ;; Initialize the fd-sets for UNIX-SELECT and return the active
          ;; descriptor count.
          (map-descriptor-handlers
           (lambda (handler)
             ;; FIXME: If HANDLER-ACTIVE ever is reinstanted, it needs
             ;; to be checked here in addition to HANDLER-BOGUS
             (unless (handler-bogus handler)
               (let ((fd (handler-descriptor handler)))
                 (ecase (handler-direction handler)
                   (:input (sb!unix:fd-set fd read-fds))
                   (:output (sb!unix:fd-set fd write-fds)))
                 (when (> fd count)
                   (setf count fd))))))
          (incf count)

          ;; Next, wait for something to happen.
          (multiple-value-bind (value err)
              (sb!unix:unix-fast-select count
                                        (sb!alien:addr read-fds)
                                        (sb!alien:addr write-fds)
                                        nil to-sec to-usec)
            #!+win32
            (declare (ignore err))
            ;; Now see what it was (if anything)
            (cond ((not value)
                   ;; Interrupted or one of the file descriptors is bad.
                   ;; FIXME: Check for other errnos. Why do we return true
                   ;; when interrupted?
                   #!-win32
                   (if (eql err sb!unix:eintr)
                       t
                       (handler-descriptors-error))
                   #!+win32
                   (handler-descriptors-error))
                  ((plusp value)
                   ;; Got something. Call file descriptor handlers
                   ;; according to the readable and writable masks
                   ;; returned by select.
                   (dolist (handler
                             (select-descriptor-handlers
                              (lambda (handler)
                                (let ((fd (handler-descriptor handler)))
                                  (ecase (handler-direction handler)
                                    (:input (sb!unix:fd-isset fd read-fds))
                                    (:output (sb!unix:fd-isset fd write-fds)))))))
                     (funcall (handler-function handler)
                              (handler-descriptor handler)))
                   t)
                  ((zerop value)
                   ;; Timeout.
                   (cond (poll
                          (funcall poll)
                          (sub-serve-event more-sec more-usec deadlinep))
                         (deadlinep
                          (signal-deadline))))))))))

#!+win32-uses-file-handles
(defvar *message-function*) ; nyef-ism...?

#!+win32-uses-file-handles
(defun sub-serve-event (to-sec to-usec deadlinep)
  (sb!alien:with-alien ((handles (array unsigned-long 63)))
    ;; NOTE: Can't have more than 63 handles (api limit).
    ;; FIXME: Enforce this.
    (let ((count 0)
          (pipe-ready nil)
          (timeout (cond ((or to-sec to-usec)
                          (+ (* (or to-sec 0) 1000)
                             (or to-usec 0)))
                         (t
                          (unless sb!sys:*interrupts-enabled*
                            (sb!unix::note-dangerous-select))
                          sb!win32::+infinite+))))

      (map-descriptor-handlers
       (lambda (handler)
         (unless (handler-bogus handler)
           (ecase (handler-direction handler)
             (:input
              (let* ((handle (handler-descriptor handler)))
                (with-alien ((avail unsigned-long))
                  (if (zerop (sb!win32:peek-named-pipe handle nil 0
                                                       nil (addr avail) nil))
                      (progn
                        (setf (deref handles count) handle)
                        (incf count))
                      (setf pipe-ready t)))))
             (:output
              ;; Don't do anything here.
              )))))
      (let ((result (if (zerop count)
                        ;; Either there are no descriptor handlers or
                        ;; they're all pipes.  We don't dare call a
                        ;; wait function.
                        (if (boundp '*message-function*)
                            ;; These results lose no matter what.
                            count
                            (progn
                              (sleep 0.5)
                              nil))
                        ;; There are valid descriptor handlers that
                        ;; aren't pipes.
                        (if (boundp '*message-function*)
                            (sb!win32::MsgWaitForMultipleObjects
                             count (addr (deref handles 0))
                             0 timeout sb!win32::+qs-allevents+)
                            (sb!win32::WaitForMultipleObjects
                             count (addr (deref handles 0))
                             0 timeout)))))
        (when pipe-ready
          (dolist (handler (select-descriptor-handlers
                            (lambda (handler)
                              (ecase (handler-direction handler)
                                (:input
                                 (with-alien ((avail unsigned-long))
                                   (and (not (zerop
                                              (sb!win32:peek-named-pipe
                                               (handler-descriptor handler)
                                               nil 0 nil (addr avail) nil)))
                                        (not (zerop avail)))))
                                (:output
                                 ;; Handles are always ready for output.
                                 t)))))
            (funcall (handler-function handler)
                     (handler-descriptor handler))
            t))
        (cond ((eq result nil))
              ((< result count)
               ;; The result'th handle in handles is ready for reading.
               (let* ((handle (deref handles result)))
                 (dolist (handler (select-descriptor-handlers
                                   (lambda (handler)
                                     (ecase (handler-direction handler)
                                       (:input
                                        (= handle (handler-descriptor handler)))
                                       (:output
                                        ;; Handles are always ready for output.
                                        t)))))
                   (funcall (handler-function handler)
                            (handler-descriptor handler))
                   t)))
              ((= result count)
               ;; There is a windows message ready.
               (funcall *message-function*)
               t)
              ((= result sb!win32::+wait-timeout+)
               ;; The timeout elapsed.
               (when deadlinep
                 (sb!sys:signal-deadline))
               nil)
              (t
               ;; Something unexpected happened, should probably error.
               (handler-descriptors-error) ;; ???
               ))))))
